package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.Empresa;
import br.com.interliga.util.FacesUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAO_Empresa {

    ConexaoBD conexao = new ConexaoBD();

    
    //SALVAR
    public void salvar(Empresa empresa) throws SQLException {
        String sql = "INSERT INTO empresa (cnpj, nome_empresa, responsavel, estado, cidade, bairro, rua, numero_casa, telefone, area_atuacao, email, senha, tipo) VALUES (?,?,?,?,?,?,?,?,?,?,?,MD5(?),?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, empresa.getCnpj());
            pst.setString(2, empresa.getNomeEmpresa());
            pst.setString(3, empresa.getResponsavel());
            pst.setString(4, empresa.getEstado());
            pst.setString(5, empresa.getCidade());
            pst.setString(6, empresa.getBairro());
            pst.setString(7, empresa.getRua());
            pst.setString(8, empresa.getNumeroCasa());
            pst.setString(9, empresa.getTelefone());
            pst.setString(10, empresa.getAreaAtuacao());
            pst.setString(11, empresa.getEmail());
            pst.setString(12, empresa.getSenha());
            pst.setString(13, empresa.getTipo());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //ATUALIZAR -  aqui só não atualiza o tipo do usuário, pq o tipo do usuário não vai mudar nunca
    public void atualizar(Empresa empresa, Integer id) throws SQLException {
        String sql = "UPDATE empresa SET  cnpj=?, nome_empresa=?, responsavel=?, estado=?, cidade=?, bairro=?, rua=?, numero_casa=?, telefone=?, area_atuacao=?, email=? WHERE id_empresa = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, empresa.getCnpj());
            pst.setString(2, empresa.getNomeEmpresa());
            pst.setString(3, empresa.getResponsavel());
            pst.setString(4, empresa.getEstado());
            pst.setString(5, empresa.getCidade());
            pst.setString(6, empresa.getBairro());
            pst.setString(7, empresa.getRua());
            pst.setString(8, empresa.getNumeroCasa());
            pst.setString(9, empresa.getTelefone());
            pst.setString(10, empresa.getAreaAtuacao());
            pst.setString(11, empresa.getEmail());
            
            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //EXCLUIR
    //OBSERVAÇÃO: ESSE MÉTODO EXLUI PRIMEIRO TODAS AS SOLICITAÇÕES DA EMPRESA E DEPOIS ELE EXCLUI DE FATO A EMPRESA
    public void excluir(Empresa empresa) throws SQLException {
        String sql1 = "DELETE solicitacao_emp_est.* FROM solicitacao_emp_est WHERE id_empresa_fk =?;";
        String sql2 = "DELETE empresa.* FROM empresa WHERE empresa.id_empresa = ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst1;
            PreparedStatement pst2;

            pst1 = conn.prepareStatement(sql1);
            pst2 = conn.prepareStatement(sql2);

            pst1.setInt(1, empresa.getIdEmpresa());
            pst2.setInt(1, empresa.getIdEmpresa());
            //FacesUtil.MensagemIformativa("Deu Certo!!!");

            pst1.execute();
            pst1.close();

            pst2.execute();
            pst2.close();

            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
            //FacesUtil.MensagemErro("Deu errado: " + a);
        }
    }

    
    //LISTAR
    public List<Empresa> listar() {
        String sql = "SELECT *from empresa;";
        List<Empresa> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idEmpresa = rs.getInt("id_empresa");
                String cnpj = rs.getString("cnpj");
                String nomeEmpresa = rs.getString("nome_empresa");
                String responsavel = rs.getString("responsavel");
                String estado = rs.getString("estado");
                String cidade = rs.getString("cidade");
                String bairro = rs.getString("bairro");
                String rua = rs.getString("rua");
                String numeroCasa = rs.getString("numero_casa");
                String telefone = rs.getString("telefone");
                String areaAtuacao = rs.getString("area_atuacao");
                String email = rs.getString("email");
                String senha = rs.getString("senha");
                String tipo = rs.getString("tipo");

                Empresa empresa = new Empresa();

                empresa.setIdEmpresa(idEmpresa);
                empresa.setCnpj(cnpj);
                empresa.setNomeEmpresa(nomeEmpresa);
                empresa.setResponsavel(responsavel);
                empresa.setEstado(estado);
                empresa.setCidade(cidade);
                empresa.setBairro(bairro);
                empresa.setRua(rua);
                empresa.setNumeroCasa(numeroCasa);
                empresa.setTelefone(telefone);
                empresa.setAreaAtuacao(areaAtuacao);
                empresa.setEmail(email);
                empresa.setSenha(senha);
                empresa.setTipo(tipo);

                lista.add(empresa);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //
    public Empresa autenticar(Empresa empresa) {
        String sql = "select * from empresa where email= '" + empresa.getEmail()
                + "' and senha = MD5('" + empresa.getSenha() + "');";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                empresa.setIdEmpresa(rs.getInt("id_empresa"));
                empresa.setTipo(rs.getString("tipo"));
                empresa.setNomeEmpresa(rs.getString("nome_empresa"));
            } else {
                empresa.setNomeEmpresa("");
                FacesUtil.MensagemErro("Usuário ou senha incorretas!");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return empresa;
    }

    
    //PREENCHE DADOS DA "EMPRESA" DE ACORDO COM OS DADOS QUE ESTÃO NO BANCO DE DADOS
    public Empresa preencheDadosEmp(Empresa empresa, Integer idEmpresa) {
        String sql = "SELECT * from empresa WHERE id_empresa = '" + idEmpresa + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                empresa.setCnpj(rs.getString("cnpj"));
                empresa.setNomeEmpresa(rs.getString("nome_empresa"));
                empresa.setResponsavel(rs.getString("responsavel"));
                empresa.setEstado(rs.getString("estado"));
                
                empresa.setCidade(rs.getString("cidade"));
                empresa.setBairro(rs.getString("bairro"));
                empresa.setRua(rs.getString("rua"));
                empresa.setNumeroCasa(rs.getString("numero_casa"));
                
                empresa.setTelefone(rs.getString("telefone"));
                empresa.setAreaAtuacao(rs.getString("area_atuacao"));
                empresa.setEmail(rs.getString("email"));
            }

            pst.close();
            ConexaoBD.Desconectar();
            return empresa;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //BUSCA O "CNPJ" DE UMA EMPRESA
    public static String buscaCnpj(Integer idEmpresa) {
        String sql = "select cnpj from empresa where id_empresa= '" + idEmpresa + "';";
        String cnpj = "";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                cnpj = rs.getString("cnpj");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return cnpj;
    }
    
    
    //BUSCA O "CNPJ" DE UMA EMPRESA
    public static String buscaEmail(Integer idEmpresa) {
        String sql = "select email from empresa where id_empresa= '" + idEmpresa + "';";
        String email = "";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                email = rs.getString("email");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return email;
    }

    
    //ATUALIZAR SOMENTE A SENHA
    public void atualizarSenha(Empresa empresa, Integer id) throws SQLException {
        String sql = "UPDATE empresa SET senha=MD5(?) WHERE id_empresa = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, empresa.getSenha());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }
    
    
}


//TESTAR METODOS
//    public static void main(String[] args) throws SQLException {
//        DAO_Empresa a = new DAO_Empresa();
//        Empresa emp = new Empresa();
//
//        emp.setCnpj("313");
//        emp.setNomeEmpresa("retail");
//        emp.setResponsavel("Kubo");
//        emp.setLogradouro("Inglaterra");
//
//        emp.setTelefone("11112222");
//        emp.setAreaAtuacao("anime");
//        emp.setEmail("anime@gmail.com");
//        emp.setSenha("dasdasasd1212121");
//        emp.setTipo("empresa");
//
//        //a.salvar(emp);
//        //a.atualizar(emp, 2);
//        //a.excluir();
//        
//        //List<Empresa> lista = a.listar();
//        //for (int i = 0; i < lista.size(); i++) {
//        //    System.out.println("" + lista.get(i).getNomeEmpresa());
//        //}
//        
//    }

