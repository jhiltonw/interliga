package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.Estagiario;
import br.com.interliga.modelo.EstagiarioPreCadastro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAO_PreCadastro {

    ConexaoBD conexao = new ConexaoBD();

    
    //SALVAR
    public void salvar(EstagiarioPreCadastro preCad) throws SQLException {
        String sql = "INSERT INTO estagiario_pre_cadastro (nome_estagiario, cpf, num_matricula, situacao, turno, curso) VALUES (?,?,?,?,?,?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, preCad.getNomeEstagiario());
            pst.setString(2, preCad.getCpf());
            pst.setString(3, preCad.getNumMatricula());
            pst.setString(4, preCad.getSituacao());
            pst.setString(5, preCad.getTurno());
            pst.setString(6, preCad.getCurso());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //ATUALIZAR
    public void atualizar(EstagiarioPreCadastro preCad, Integer id) throws SQLException {
        String sql = "UPDATE estagiario_pre_cadastro SET  nome_estagiario=?, cpf=?, num_matricula=?, situacao=?, turno=?, curso=? WHERE id_pre_cad = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, preCad.getNomeEstagiario());
            pst.setString(2, preCad.getCpf());
            pst.setString(3, preCad.getNumMatricula());
            pst.setString(4, preCad.getSituacao());
            pst.setString(5, preCad.getTurno());
            pst.setString(6, preCad.getCurso());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //EXCLUIR
    public void excluir(EstagiarioPreCadastro preCad) throws SQLException {
        String sql = "DELETE FROM estagiario_pre_cadastro WHERE id_pre_cad = ?";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            pst.setInt(1, preCad.getIdPreCad());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //LISTAR
    public List<EstagiarioPreCadastro> listar() {
        String sql = "SELECT *from estagiario_pre_cadastro;";
        List<EstagiarioPreCadastro> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idPreCad = rs.getInt("id_pre_cad");
                String nomeEstagiario = rs.getString("nome_estagiario");
                String cpf = rs.getString("cpf");
                String numMatricula = rs.getString("num_matricula");
                String situacao = rs.getString("situacao");
                String turno = rs.getString("turno");
                String curso = rs.getString("curso");

                EstagiarioPreCadastro preCad = new EstagiarioPreCadastro();

                preCad.setIdPreCad(idPreCad);
                preCad.setNomeEstagiario(nomeEstagiario);
                preCad.setCpf(cpf);
                preCad.setNumMatricula(numMatricula);
                preCad.setSituacao(situacao);
                preCad.setTurno(turno);
                preCad.setCurso(curso);

                lista.add(preCad);
            }

            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //PESQUISAR POR "NÚMERO DE MATRICULA" 
    //E RETORNA UM "BOOLEAN" QUE DIZ SE EXISTE UM "PRÉ-CADASTRO" COM AQUELE "NÚMERO DE MATRÍCULA"
    public boolean pesquisarMatricula(String numMatricula) {
        String sql = "SELECT * from estagiario_pre_cadastro WHERE num_matricula = '" + numMatricula + "';";
        boolean cadastroLiberado = false;
        String numMatriculaAux = null;

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                numMatriculaAux = rs.getString("num_matricula");
            }

            if (numMatriculaAux != null) {
                cadastroLiberado = true;
            }

            pst.close();
            ConexaoBD.Desconectar();
            return cadastroLiberado;

        } catch (Exception e) {
            e.printStackTrace();
            return cadastroLiberado;
        }
    }

    
    //PREENCHE DADOS DO "ESTAGIÁRIO" DE ACORDO COM OS DADOS QUE ESTÃO NO "PRÉ-CADASTRO DO ESTAGIÁRIO"
    public Estagiario preencheDadosEst(String numMatricula, Estagiario estagiario) {
        String sql = "SELECT * from estagiario_pre_cadastro WHERE num_matricula = '" + numMatricula + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                estagiario.setNomeEstagiario(rs.getString("nome_estagiario"));
                estagiario.setCpf(rs.getString("cpf"));
                estagiario.setNumMatricula(rs.getString("num_matricula"));
                estagiario.setSituacao(rs.getString("situacao"));
                estagiario.setTurnoEstudo(rs.getString("turno"));
                estagiario.setCurso(rs.getString("curso"));
            }

            pst.close();
            ConexaoBD.Desconectar();
            return estagiario;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
}

//TESTAR METODOS
//    public static void main(String[] args) throws SQLException {
//        DAO_PreCadastro a = new DAO_PreCadastro();
//        EstagiarioPreCadastro preCad = new EstagiarioPreCadastro();
//
//        preCad.setNomeEstagiario("Kung Lao");
//        preCad.setCpf("456465");
//        preCad.setNumMatricula("54654");
//        preCad.setSituacao("ativo");
//        preCad.setTurno("tarde");
//        preCad.setCurso("biologia");
//
//        a.salvar(preCad);
//        a.atualizar(preCad, 4);
//        a.excluir(preCad);
//        List<EstagiarioPreCadastro> lista = a.listar();
//        System.out.println(lista.get(0).getNomeEstagiario());
//        a.excluir(lista.get(2));
//        for (int i = 0; i < lista.size(); i++) {
//            System.out.println("" + lista.get(i).getNomeEstagiario());
//        }
//        System.out.println(a.pesquisarMatricula("qqqqq")); ESSE TESTA O METODO PESQUISAR POR MATRICULA
//    }
