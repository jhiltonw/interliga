package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.Administrador;
import br.com.interliga.util.FacesUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAO_Administrador {

    ConexaoBD conexao = new ConexaoBD();

    
    //SALVAR
    public void salvar(Administrador adm) throws SQLException {
        String sql = "INSERT INTO administrador (nome, cpf, rg, sexo, email, senha, tipo) VALUES (?,?,?,?,?,MD5(?),?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, adm.getNome());
            pst.setString(2, adm.getCpf());
            pst.setString(3, adm.getRg());
            pst.setString(4, adm.getSexo());
            pst.setString(5, adm.getEmail());
            pst.setString(6, adm.getSenha());
            pst.setString(7, adm.getTipo());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //ATUALIZAR - aqui só não atualiza o tipo do usuário, pq o tipo do usuário não vai mudar nunca
    public void atualizar(Administrador adm, Integer id) throws SQLException {
        String sql = "UPDATE administrador SET  nome=?, cpf=?, rg=?, sexo=?, email=? WHERE id_adm = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, adm.getNome());
            pst.setString(2, adm.getCpf());
            pst.setString(3, adm.getRg());
            pst.setString(4, adm.getSexo());
            pst.setString(5, adm.getEmail());
            
            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
            //FacesUtil.MensagemErro("" + a);
        }
    }
    
    
    //ATUALIZAR SOMENTE A SENHA
    public void atualizarSenha(Administrador administrador, Integer id) throws SQLException {
        String sql = "UPDATE administrador SET senha=MD5(?) WHERE id_adm = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, administrador.getSenha());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //EXCLUIR
    public void excluir(Administrador adm) throws SQLException {
        String sql = "DELETE FROM administrador WHERE id_adm= ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            pst.setInt(1, adm.getIdAdm());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //LISTAR
    public List<Administrador> listar() {
        String sql = "SELECT *from administrador;";
        List<Administrador> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idAdm = rs.getInt("id_adm");
                String nome = rs.getString("nome");
                String cpf = rs.getString("cpf");
                String rg = rs.getString("rg");
                String sexo = rs.getString("sexo");
                String email = rs.getString("email");
                String tipo = rs.getString("tipo");

                Administrador adm = new Administrador();

                adm.setIdAdm(idAdm);
                adm.setNome(nome);
                adm.setCpf(cpf);
                adm.setRg(rg);
                adm.setSexo(sexo);
                adm.setEmail(email);
                adm.setTipo(tipo);

                lista.add(adm);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    public Administrador autenticar(Administrador adm) {
        String sql = "select * from administrador where email= '" + adm.getEmail()
                + "' and senha = MD5('" + adm.getSenha() + "');";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                adm.setIdAdm(rs.getInt("id_adm"));
                adm.setTipo(rs.getString("tipo"));
                adm.setNome(rs.getString("nome"));
            } else {
                adm.setNome("");
                FacesUtil.MensagemErro("Usuário ou senha incorretas!");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return adm;
    }
    

}
