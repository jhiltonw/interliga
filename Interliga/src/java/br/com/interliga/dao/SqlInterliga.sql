/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Igor
 * Created: 01/05/2019
 */

/*Tabela Administrador*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`administrador` (
  `id_adm` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `cpf` VARCHAR(255) NOT NULL,
  `rg` VARCHAR(255) NOT NULL,
  `sexo` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `tipo` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_adm`),
  UNIQUE (`cpf`),
  UNIQUE (`rg`));

/*Tabela Empresa*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`empresa` (
  `id_empresa` INT NOT NULL AUTO_INCREMENT,
  `cnpj` VARCHAR(255) NOT NULL,
  `nome_empresa` VARCHAR(255) NOT NULL,
  `responsavel` VARCHAR(255) NOT NULL,
  `estado` VARCHAR(255) NOT NULL,
  `cidade` VARCHAR(255) NOT NULL,
  `bairro` VARCHAR(255) NOT NULL,
  `rua` VARCHAR(255) NOT NULL,
  `numero_casa` VARCHAR(255) NOT NULL,
  `telefone` VARCHAR(255) NOT NULL,  
  `area_atuacao` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `tipo` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_empresa`),
  UNIQUE (`cnpj`));

/*Tabela EstagiarioPreCadastro*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`estagiario_pre_cadastro` (
  `id_pre_cad` INT NOT NULL AUTO_INCREMENT,
  `nome_estagiario` VARCHAR(255) NOT NULL,    
  `cpf` VARCHAR(255) NOT NULL,
  `num_matricula` VARCHAR(255) NOT NULL,
  `situacao` VARCHAR(255) NOT NULL,
  `turno` VARCHAR(255) NOT NULL,  
  `curso` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_pre_cad`),
  UNIQUE (`cpf`),
  UNIQUE (`num_matricula`));

/*Tabela Estagiario*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`estagiario` (
  `id_estagiario` INT NOT NULL AUTO_INCREMENT,
  `nome_estagiario` VARCHAR(255) NOT NULL,
  `cpf` VARCHAR(255) NOT NULL,
  `rg` VARCHAR(255) NOT NULL,
  `num_matricula` VARCHAR(255) NOT NULL,
  `curso` VARCHAR(255) NOT NULL,
  `universidade` VARCHAR(255) NOT NULL,
  `turno_estudo` VARCHAR(255) NOT NULL,
  `sexo` VARCHAR(255) NOT NULL,  
  `email` VARCHAR(255) NOT NULL,
  `senha` VARCHAR(255) NOT NULL,
  `situacao` VARCHAR(255) NOT NULL,
  `tipo` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_estagiario`),
  UNIQUE (`cpf`),
  UNIQUE (`num_matricula`),
  UNIQUE (`rg`));

/*Tabela Curriculo*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`curriculo` (
  `id_curriculo` INT NOT NULL AUTO_INCREMENT,
  `nome_estagiario` VARCHAR(255) NOT NULL,
  `cpf` VARCHAR(255) NOT NULL,
  `curso` VARCHAR(255) NOT NULL,
  `estado` VARCHAR(255) NOT NULL,
  `cidade` VARCHAR(255) NOT NULL,
  `bairro` VARCHAR(255) NOT NULL,
  `rua` VARCHAR(255) NOT NULL,
  `numero_casa` VARCHAR(255) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `sexo` VARCHAR(255) NOT NULL,
  `area_atuacao` VARCHAR(255) NOT NULL,
  `turno_trabalho` VARCHAR(255) NOT NULL,
  `idiomas` VARCHAR(255) NOT NULL,
  `informatica` VARCHAR(255) NOT NULL,
  `exp_profissional` TEXT NULL DEFAULT NULL,
  `cursos_extras` TEXT NULL DEFAULT NULL,
  `informacoes_adicionais` TEXT NULL DEFAULT NULL,
  `id_estagiario_fk` INT,
  PRIMARY KEY (`id_curriculo`),
  CONSTRAINT FK_CurriculoEstag FOREIGN KEY (`id_estagiario_fk`) REFERENCES `interliga_upe`.`estagiario` (`id_estagiario`) ON DELETE CASCADE,
  UNIQUE (`cpf`));

/*Tabela Solicitacao Empresa Estagiariop*/
CREATE TABLE IF NOT EXISTS `interliga_upe`.`solicitacao_emp_est` (
  `id_solicitacao` INT NOT NULL AUTO_INCREMENT,
  `id_empresa_fk` INT,
  `id_estagiario_fk` INT,
  `nome_empresa` VARCHAR(255) NULL DEFAULT NULL,
  `cnpj` VARCHAR(255) NULL DEFAULT NULL,
  `nome_estagiario` VARCHAR(255) NULL DEFAULT NULL,
  `num_matricula` VARCHAR(255) NULL DEFAULT NULL,
  `area_de_atuacao` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_solicitacao`),
  CONSTRAINT fk_empresa FOREIGN KEY (`id_empresa_fk`) REFERENCES `interliga_upe`.`empresa` (`id_empresa`),
  CONSTRAINT fk_estagiario FOREIGN KEY (`id_estagiario_fk`) REFERENCES `interliga_upe`.`estagiario` (`id_estagiario`));
  



