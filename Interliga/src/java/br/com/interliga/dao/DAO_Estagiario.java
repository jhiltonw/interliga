package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.Estagiario;
import br.com.interliga.util.FacesUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAO_Estagiario {

    ConexaoBD conexao = new ConexaoBD();

    
    //SALVAR
    public void salvar(Estagiario estagiario) throws SQLException {
        String sql = "INSERT INTO estagiario (nome_estagiario, cpf, rg, num_matricula, curso, universidade, turno_estudo, sexo, email, senha, situacao, tipo) VALUES (?,?,?,?,?,?,?,?,?,MD5(?),?,?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, estagiario.getNomeEstagiario());
            pst.setString(2, estagiario.getCpf());
            pst.setString(3, estagiario.getRg());
            pst.setString(4, estagiario.getNumMatricula());
            pst.setString(5, estagiario.getCurso());
            pst.setString(6, estagiario.getUniversidade());
            pst.setString(7, estagiario.getTurnoEstudo());
            pst.setString(8, estagiario.getSexo());
            pst.setString(9, estagiario.getEmail());
            pst.setString(10, estagiario.getSenha());
            pst.setString(11, estagiario.getSituacao());
            pst.setString(12, estagiario.getTipo());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }
    

    //ATUALIZAR
    //OBSERVAÇÃO: QUANDO OS DADOS (NOME, CPF, CURSO, SEXO) SÃO ATUALIZADOS NO CASDASTRO DO ESTAGIÁRIO, ELES SERAM REPLICADOS PARA O CURERÍCULO TBM
    public void atualizar(Estagiario estagiario, Integer id) throws SQLException {
        String sql1 = "UPDATE estagiario SET  nome_estagiario=?, cpf=?, rg=?, num_matricula=?, curso=?, universidade=?, turno_estudo=?, sexo=?, email=?, situacao=? WHERE id_estagiario = '" + id + "';";
        String sql2 = "UPDATE curriculo SET  nome_estagiario=?, cpf=?, curso=?, sexo=?  WHERE id_estagiario_fk = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst1;
            PreparedStatement pst2;
            
            pst1 = conn.prepareStatement(sql1);
            pst2 = conn.prepareStatement(sql2);
            
            pst1.setString(1, estagiario.getNomeEstagiario());
            pst1.setString(2, estagiario.getCpf());
            pst1.setString(3, estagiario.getRg());
            pst1.setString(4, estagiario.getNumMatricula());
            pst1.setString(5, estagiario.getCurso());
            pst1.setString(6, estagiario.getUniversidade());
            pst1.setString(7, estagiario.getTurnoEstudo());
            pst1.setString(8, estagiario.getSexo());
            pst1.setString(9, estagiario.getEmail());
            pst1.setString(10, estagiario.getSituacao());
            
            pst2.setString(1, estagiario.getNomeEstagiario());
            pst2.setString(2, estagiario.getCpf());
            pst2.setString(3, estagiario.getCurso());
            pst2.setString(4, estagiario.getSexo());
            
            pst1.execute();
            pst1.close();
            
            pst2.execute();
            pst2.close();
            
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //EXCLUIR
    public void excluir(Estagiario estagiario) throws SQLException {

        String sql1 = "DELETE solicitacao_emp_est.* FROM solicitacao_emp_est WHERE solicitacao_emp_est.id_estagiario_fk= ?;";
        String sql2 = "DELETE curriculo.* FROM curriculo WHERE curriculo.id_estagiario_fk= ?;";
        String sql3 = "DELETE estagiario.* FROM estagiario WHERE estagiario.id_estagiario= ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst1;
            PreparedStatement pst2;
            PreparedStatement pst3;

            pst1 = conn.prepareStatement(sql1);
            pst2 = conn.prepareStatement(sql2);
            pst3 = conn.prepareStatement(sql3);

            pst1.setInt(1, estagiario.getIdEstagiario());
            pst2.setInt(1, estagiario.getIdEstagiario());
            pst3.setInt(1, estagiario.getIdEstagiario());

            pst1.execute();
            pst1.close();
            //FacesUtil.MensagemIformativa("Deu Certo 1!!!");
            
            pst2.execute();
            pst2.close();
            //FacesUtil.MensagemIformativa("Deu Certo 2!!!");

            pst3.execute();
            pst3.close();
            //FacesUtil.MensagemIformativa("Deu Certo 3!!!");

            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //LISTAR
    public List<Estagiario> listar() {
        String sql = "SELECT *from estagiario;";
        List<Estagiario> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idEstagiario = rs.getInt("id_estagiario");
                String nomeEstagiario = rs.getString("nome_estagiario");
                String cpf = rs.getString("cpf");
                String rg = rs.getString("rg");
                String numMatricula = rs.getString("num_matricula");
                String curso = rs.getString("curso");
                String universidade = rs.getString("universidade");
                String turnoEstudo = rs.getString("turno_estudo");
                String sexo = rs.getString("sexo");
                String email = rs.getString("email");
                String senha = rs.getString("senha");
                String situacao = rs.getString("situacao");
                String tipo = rs.getString("tipo");

                Estagiario estagiario = new Estagiario();

                estagiario.setIdEstagiario(idEstagiario);
                estagiario.setNomeEstagiario(nomeEstagiario);
                estagiario.setCpf(cpf);
                estagiario.setRg(rg);
                estagiario.setNumMatricula(numMatricula);
                estagiario.setCurso(curso);
                estagiario.setUniversidade(universidade);
                estagiario.setTurnoEstudo(turnoEstudo);
                estagiario.setSexo(sexo);
                estagiario.setEmail(email);
                estagiario.setSenha(senha);
                estagiario.setSituacao(situacao);
                estagiario.setTipo(tipo);

                lista.add(estagiario);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //ESSE METODO PESQUISA UM "ESTAGIÁRIO" POR "EMAIL" "SENHA" E SETA NA SESSÃO OS DADOS "ID", "TIPO", "NOME" 
    public Estagiario autenticar(Estagiario estagiario) {
        String sql = "select * from estagiario where email= '" + estagiario.getEmail()
                + "' and senha = MD5('" + estagiario.getSenha() + "');";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                estagiario.setIdEstagiario(rs.getInt("id_estagiario"));
                estagiario.setTipo(rs.getString("tipo"));
                estagiario.setNomeEstagiario(rs.getString("nome_estagiario"));
            } else {
                estagiario.setNomeEstagiario("");
                FacesUtil.MensagemErro("Usuário ou senha incorretas!");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return estagiario;
    }

    
    //PREENCHE DADOS DO "ESTAGIÁRIO" DE ACORDO COM OS DADOS QUE ESTÃO NO BANCO DE DADOS
    public Estagiario preencheDadosEst1(Estagiario estagiario, Integer idEstagiario) {
        String sql = "SELECT * from estagiario WHERE id_estagiario = '" + idEstagiario + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                estagiario.setNomeEstagiario(rs.getString("nome_estagiario"));
                estagiario.setCpf(rs.getString("cpf"));
                estagiario.setRg(rs.getString("rg"));
                estagiario.setNumMatricula(rs.getString("num_matricula"));
                estagiario.setCurso(rs.getString("curso"));

                estagiario.setUniversidade(rs.getString("universidade"));
                estagiario.setTurnoEstudo(rs.getString("turno_estudo"));
                estagiario.setSexo(rs.getString("sexo"));
                estagiario.setEmail(rs.getString("email"));
                estagiario.setSituacao(rs.getString("situacao"));

                //estagiario.setSenha(rs.getString("senha")); //ACREDITO QUE SEJA MELHOR CRIAR UMA TELA ESPECIFICA PARA ALTERAR ESSE TIPO DE DADO   
            }

            pst.close();
            ConexaoBD.Desconectar();
            return estagiario;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //BUSCA O "NÚMERO DE MATRÍCULA" DE UM ESTAGIARIO
    public static String buscaNumMatricula(Integer idEstagiario) {
        String sql = "select num_matricula from estagiario where id_estagiario= '" + idEstagiario + "';";
        String numMatricula = "";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                numMatricula = rs.getString("num_matricula");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return numMatricula;
    }

    
    //BUSCA O "NÚMERO DE MATRÍCULA" DE UM ESTAGIARIO
    public static String buscaEmail(Integer idEstagiario) {
        String sql = "select email from estagiario where id_estagiario= '" + idEstagiario + "';";
        String email = "";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                email = rs.getString("email");
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return email;
    }
    
    
    //ATUALIZAR SOMENTE A SENHA
    public void atualizarSenha(Estagiario estagiario, Integer id) throws SQLException {
        String sql = "UPDATE estagiario SET  senha=MD5(?) WHERE id_estagiario = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, estagiario.getSenha());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
}
//TESTAR METODOS
//    public static void main(String[] args) throws SQLException {
//        DAO_Estagiario a = new DAO_Estagiario();
//        Estagiario estagiario = new Estagiario();
//        
//        estagiario.setNomeEstagiario("Hanzo Hazachi");
//        estagiario.setCpf("123123");
//        estagiario.setRg("121212");
//        estagiario.setNumMatricula("14124");
//        estagiario.setCurso("geografia");
//        estagiario.setUniversidade("UFAL");
//        estagiario.setTurnoEstudo("noite");
//        estagiario.setSexo("masculino");
//        estagiario.setEmail("fogo@gmail.com");
//        estagiario.setSenha("lava");
//        estagiario.setTipo("estagiario");
//        
//        //a.salvar(estagiario);
//        //a.atualizar(estagiario, 3);
//        //a.excluir();
//        
//        //List<Estagiario> lista = a.listar();
//        //for (int i = 0; i < lista.size(); i++) {
//        //    System.out.println("" + lista.get(i).getNomeEstagiario());
//        //}
//        
//    }

