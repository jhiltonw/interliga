package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.SolicitacaoEmpEst;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DAO_Solicitacao {

    ConexaoBD conexao = new ConexaoBD();

    //SALVAR
    public void salvar(SolicitacaoEmpEst solicitEmpEst) throws SQLException {
        String sql = "INSERT INTO solicitacao_emp_est (id_empresa_fk, id_estagiario_fk, nome_empresa, cnpj, nome_estagiario, num_matricula, area_de_atuacao) VALUES (?,?,?,?,?,?,?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setInt(1, solicitEmpEst.getIdEmpresa());
            pst.setInt(2, solicitEmpEst.getIdEstagiario());
            pst.setString(3, solicitEmpEst.getNomeEmpresa());
            pst.setString(4, solicitEmpEst.getCnpj());
            pst.setString(5, solicitEmpEst.getNomeEstagiario());
            pst.setString(6, solicitEmpEst.getNumMatricula());
            pst.setString(7, solicitEmpEst.getAreaAtuacao());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //ATUALIZAR
    public void atualizar(SolicitacaoEmpEst solicitEmpEst, Integer id) throws SQLException {
        String sql = "UPDATE solicitacao_emp_est SET  id_empresa_fk=?, id_estagiario_fk=?, nome_empresa=?, cnpj=?, nome_estagiario=?, num_matricula=?, area_de_atuacao=? WHERE id_solicitacao = '" + id + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setInt(1, solicitEmpEst.getIdEmpresa());
            pst.setInt(2, solicitEmpEst.getIdEstagiario());
            pst.setString(3, solicitEmpEst.getNomeEmpresa());
            pst.setString(4, solicitEmpEst.getCnpj());
            pst.setString(5, solicitEmpEst.getNomeEstagiario());
            pst.setString(6, solicitEmpEst.getNumMatricula());
            pst.setString(7, solicitEmpEst.getAreaAtuacao());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    //EXCLUIR
    public void excluir(SolicitacaoEmpEst solicitEmpEst) throws SQLException {
        String sql = "DELETE FROM solicitacao_emp_est WHERE id_solicitacao= ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            pst.setInt(1, solicitEmpEst.getIdSolicitacao());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //LISTAR TUDO - NO CASO: ESSE MÉTODO LISTAR SERVE APENAS PARA O ADM
    public List<SolicitacaoEmpEst> listar() {
        String sql = "SELECT *from solicitacao_emp_est;";
        List<SolicitacaoEmpEst> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idSolicitacao = rs.getInt("id_solicitacao");
                int idEmpresa = rs.getInt("id_empresa_fk");
                int idEstagiario = rs.getInt("id_estagiario_fk");
                String nomeEmpresa = rs.getString("nome_empresa");
                String cnpj = rs.getString("cnpj");
                String nomeEstagiario = rs.getString("nome_estagiario");
                String numMatricula = rs.getString("num_matricula");
                String areaAtuacao = rs.getString("area_de_atuacao");

                SolicitacaoEmpEst solicitEmpEst = new SolicitacaoEmpEst();

                solicitEmpEst.setIdSolicitacao(idSolicitacao);
                solicitEmpEst.setIdEmpresa(idEmpresa);
                solicitEmpEst.setIdEstagiario(idEstagiario);
                solicitEmpEst.setNomeEmpresa(nomeEmpresa);
                solicitEmpEst.setCnpj(cnpj);
                solicitEmpEst.setNomeEstagiario(nomeEstagiario);
                solicitEmpEst.setNumMatricula(numMatricula);
                solicitEmpEst.setAreaAtuacao(areaAtuacao);

                lista.add(solicitEmpEst);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //VERIFICA SE DETERMINADO UMA EMPRESA JÁ SOLICITOU DETERMINADO ESTAGIARIO
    public boolean existeSolicitacao(Integer idEmpresa, Integer idEstagiario) {
        String sql = "SELECT * from solicitacao_emp_est WHERE id_empresa_fk= '" + idEmpresa + "' and id_estagiario_fk= '" + idEstagiario + "';";
        boolean existe = false;

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                existe = true;
            } else {
                existe = false;
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return existe;
    }

    
    //LISTA TODAS AS SOLICITAÇÕES FEITAS POR UMA DETERMINADA EMPRESA
    public List<SolicitacaoEmpEst> solicitFeitasEmp(Integer idEmpresa) {
        String sql = "SELECT * FROM solicitacao_emp_est WHERE id_empresa_fk = '" + idEmpresa + "';";
        List<SolicitacaoEmpEst> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idSolicitacao = rs.getInt("id_solicitacao");
                String nomeEstagiario = rs.getString("nome_estagiario");
                String numMatricula = rs.getString("num_matricula");
                String areaAtuacao = rs.getString("area_de_atuacao");

                SolicitacaoEmpEst solicitEmpEst = new SolicitacaoEmpEst();

                solicitEmpEst.setIdSolicitacao(idSolicitacao);
                solicitEmpEst.setNomeEstagiario(nomeEstagiario);
                solicitEmpEst.setNumMatricula(numMatricula);
                solicitEmpEst.setAreaAtuacao(areaAtuacao);

                lista.add(solicitEmpEst);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //LISTA TODAS AS SOLICITAÇÕES RECEBIDAS POR UM DETERMINADO ESTAGIARIO
    public List<SolicitacaoEmpEst> solicitRecebidasEst(Integer isEstagiario) {
        String sql = "SELECT * FROM solicitacao_emp_est WHERE id_estagiario_fk = '" + isEstagiario + "';";
        List<SolicitacaoEmpEst> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                String nomeEmpresa = rs.getString("nome_empresa");
                String cnpj = rs.getString("cnpj");

                SolicitacaoEmpEst solicitEmpEst = new SolicitacaoEmpEst();

                solicitEmpEst.setNomeEmpresa(nomeEmpresa);
                solicitEmpEst.setCnpj(cnpj);

                lista.add(solicitEmpEst);
            }
            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //TESTAR METODOS
//    public static void main(String[] args) throws SQLException {
//        DAO_Solicitacao a = new DAO_Solicitacao();
//        SolicitacaoEmpEst solicitacao = new SolicitacaoEmpEst();
//        
//        solicitacao.setIdEmpresa(2);
//        solicitacao.setIdEstagiario(3);
//        solicitacao.setNomeEmpresa("sonsdadasg");//
//        solicitacao.setCnpj("452");
//        solicitacao.setNomeEstagiario("asdf ");
//        solicitacao.setNumMatricula("14124");
//        solicitacao.setAreaAtuacao("marketing");
//        
//        //a.salvar(solicitacao);
//        //a.atualizar(solicitacao, 4);
//        //a.excluir();
////        List<SolicitacaoEmpEst> lista = a.listar();
////        for (int i = 0; i < lista.size(); i++) {
////            System.out.println("" + lista.get(i).getNomeEmpresa());
////            System.out.println("" + lista.get(i).getNomeEstagiario());
////            
////        }
//    }
}
