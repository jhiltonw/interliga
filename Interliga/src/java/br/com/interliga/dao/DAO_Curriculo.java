/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.dao;

import br.com.interliga.conexao.ConexaoBD;
import br.com.interliga.modelo.Curriculo;
import br.com.interliga.util.ConverteData;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import br.com.interliga.util.ConverteData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Igor
 */
public class DAO_Curriculo {

    ConexaoBD conexao = new ConexaoBD();

    
    //SALVAR
    public void salvar(Curriculo curriculo, Integer id) throws SQLException {
        String sql = "INSERT INTO curriculo (nome_estagiario, cpf, curso, estado, cidade, bairro, rua, numero_casa, data_nascimento, sexo, area_atuacao, turno_trabalho, idiomas, informatica, exp_profissional, cursos_extras, informacoes_adicionais, id_estagiario_fk) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, curriculo.getNomeEstagiario());
            pst.setString(2, curriculo.getCpf());
            pst.setString(3, curriculo.getCurso());
            pst.setString(4, curriculo.getEstado());
            pst.setString(5, curriculo.getCidade());
            pst.setString(6, curriculo.getBairro());
            pst.setString(7, curriculo.getRua());
            pst.setString(8, curriculo.getNumeroCasa());
            pst.setDate(9, curriculo.getDataFinal());
            pst.setString(10, curriculo.getSexo());
            pst.setString(11, curriculo.getAreaAtuacao());
            pst.setString(12, curriculo.getTurnoTrabalho());
            pst.setString(13, curriculo.getIdiomas());
            pst.setString(14, curriculo.getInformatica());
            pst.setString(15, curriculo.getExpProfissional());
            pst.setString(16, curriculo.getCursosExtras());
            pst.setString(17, curriculo.getInformacoesAdicionais());
            pst.setInt(18, id);//AQUI SALVA A CHAVE ESTRANGEIRA

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //ATUALIZAR
    public void atualizar(Curriculo curriculo) throws SQLException {
        String sql = "UPDATE curriculo SET estado=?, cidade=?, bairro=?, rua=?, numero_casa=?, data_nascimento=?, area_atuacao=?, turno_trabalho=?, idiomas=?, informatica=?, exp_profissional=?, cursos_extras=?, informacoes_adicionais=? WHERE id_curriculo = '" + curriculo.getIdCurriculo() + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);

            pst.setString(1, curriculo.getEstado());
            pst.setString(2, curriculo.getCidade());
            pst.setString(3, curriculo.getBairro());
            pst.setString(4, curriculo.getRua());
            pst.setString(5, curriculo.getNumeroCasa());
            pst.setDate(6, curriculo.getDataFinal());
            pst.setString(7, curriculo.getAreaAtuacao());
            pst.setString(8, curriculo.getTurnoTrabalho());
            pst.setString(9, curriculo.getIdiomas());
            pst.setString(10, curriculo.getInformatica());
            pst.setString(11, curriculo.getExpProfissional());
            pst.setString(12, curriculo.getCursosExtras());
            pst.setString(13, curriculo.getInformacoesAdicionais());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //EXCLUIR
    public void excluir(Curriculo curriculo) throws SQLException {
        String sql = "DELETE FROM curriculo WHERE id_curriculo= ?;";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;

            pst = conn.prepareStatement(sql);
            pst.setInt(1, curriculo.getIdCurriculo());

            pst.execute();
            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }
    }

    
    //PREENCHE DADOS DO "CURRICULO" DE ACORDO COM OS DADOS QUE ESTÃO NO "CADASTRO DO ESTAGIARIO"
    //POR ISSO É USADO: QUANDO O USUÁRIO AINDA NÃO TEM CURRICULO E VAI CRIÁ-LO
    public Curriculo preencheDadosCurr1(Curriculo curriculo, Integer idEstagiario) {
        String sql = "SELECT * from estagiario WHERE id_estagiario = '" + idEstagiario + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                curriculo.setNomeEstagiario(rs.getString("nome_estagiario"));
                curriculo.setCpf(rs.getString("cpf"));
                curriculo.setCurso(rs.getString("curso"));
                curriculo.setSexo(rs.getString("sexo"));
            }

            pst.close();
            ConexaoBD.Desconectar();
            return curriculo;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //PREENCHE DADOS DO "CURRICULO" DE ACORDO COM OS DADOS QUE ESTÃO NO BANCO DE DADOS
    //POR ISSO É USADO: QUANDO O USUÁRIO JÁ TEM O CURRICULO E PRETENDE EDITÁ-LO
    public Curriculo preencheDadosCurr2(Curriculo curriculo, Integer idEstagiario) {
        String sql = "SELECT * from curriculo WHERE id_estagiario_fk = '" + idEstagiario + "';";

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                curriculo.setIdCurriculo(rs.getInt("id_curriculo"));
                curriculo.setNomeEstagiario(rs.getString("nome_estagiario"));
                curriculo.setCpf(rs.getString("cpf"));
                curriculo.setCurso(rs.getString("curso"));
                curriculo.setEstado(rs.getString("estado"));

                curriculo.setCidade(rs.getString("cidade"));
                curriculo.setBairro(rs.getString("bairro"));
                curriculo.setRua(rs.getString("rua"));
                curriculo.setNumeroCasa(rs.getString("numero_casa"));
                curriculo.setDataNascimento(ConverteData.converterParaString(rs.getDate("data_nascimento")));

                curriculo.setSexo(rs.getString("sexo"));
                curriculo.setAreaAtuacao(rs.getString("area_atuacao"));
                curriculo.setTurnoTrabalho(rs.getString("turno_trabalho"));
                curriculo.setIdiomas(rs.getString("idiomas"));
                curriculo.setInformatica(rs.getString("informatica"));

                curriculo.setExpProfissional(rs.getString("exp_profissional"));
                curriculo.setCursosExtras(rs.getString("cursos_extras"));
                curriculo.setInformacoesAdicionais(rs.getString("informacoes_adicionais"));
            }

            pst.close();
            ConexaoBD.Desconectar();
            return curriculo;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //VERIFICA SE DETERMINADO USUÁRIO JÁ POSSUI UM CURRICULO CADASTRADO
    public boolean existeCurriculo(Integer idEstagiario) {
        String sql = "SELECT * from curriculo WHERE id_estagiario_fk = '" + idEstagiario + "';";
        boolean existe = false;

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            if (rs.next()) {
                existe = true;
            } else {
                existe = false;
            }

            pst.close();
            ConexaoBD.Desconectar();

        } catch (SQLException a) {
            a.printStackTrace();
        }

        return existe;
    }

    
    //LISTA TODOS OS CURRICULOS DE ACORDO COM O "TURNO DE TRABALHO" INFORMADO
    public List<Curriculo> pesquisarCurriculosPor(String areaAtuacao, String turnoTrabalho) {
        String sql = "SELECT * FROM curriculo WHERE area_atuacao='" + areaAtuacao + "' and turno_trabalho='"+  turnoTrabalho +"';";
        List<Curriculo> lista = new ArrayList<>();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                int idCurriculo = rs.getInt("id_curriculo");
                String nomeEstagiario = rs.getString("nome_estagiario");
                String cpf = rs.getString("cpf");
                String curso = rs.getString("curso");
                int idEstagiarioFK = rs.getInt("id_estagiario_fk");

                Curriculo curriculo = new Curriculo();

                curriculo.setIdCurriculo(idCurriculo);
                curriculo.setNomeEstagiario(nomeEstagiario);
                curriculo.setCpf(cpf);
                curriculo.setCurso(curso);
                curriculo.setIdEstagiarioFK(idEstagiarioFK);

                lista.add(curriculo);
            }

            pst.close();
            ConexaoBD.Desconectar();
            return lista;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
    //PESQUISA UM CURRICULO DE ACORDO COM O "ID_CURRICULO" INFORMADO
    public Curriculo pesquisarPorId(Integer idCurriculo) {
        String sql = "SELECT * FROM curriculo WHERE id_curriculo='" + idCurriculo + "';";

        Curriculo curriculo = new Curriculo();

        try {
            Connection conn = ConexaoBD.Conectar();
            PreparedStatement pst;
            ResultSet rs;

            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery(sql);

            while (rs.next()) {
                curriculo.setIdCurriculo(rs.getInt("id_curriculo"));
                curriculo.setNomeEstagiario(rs.getString("nome_estagiario"));
                curriculo.setCpf(rs.getString("cpf"));
                curriculo.setCurso(rs.getString("curso"));
                curriculo.setEstado(rs.getString("estado"));

                curriculo.setCidade(rs.getString("cidade"));
                curriculo.setBairro(rs.getString("bairro"));
                curriculo.setRua(rs.getString("rua"));
                curriculo.setNumeroCasa(rs.getString("numero_casa"));
                curriculo.setDataNascimento(ConverteData.converterParaString(rs.getDate("data_nascimento")));

                curriculo.setSexo(rs.getString("sexo"));
                curriculo.setAreaAtuacao(rs.getString("area_atuacao"));
                curriculo.setTurnoTrabalho(rs.getString("turno_trabalho"));
                curriculo.setIdiomas(rs.getString("idiomas"));
                curriculo.setInformatica(rs.getString("informatica"));

                curriculo.setExpProfissional(rs.getString("exp_profissional"));
                curriculo.setCursosExtras(rs.getString("cursos_extras"));
                curriculo.setInformacoesAdicionais(rs.getString("informacoes_adicionais"));
                curriculo.setIdEstagiarioFK(rs.getInt("id_estagiario_fk"));
            }

            pst.close();
            ConexaoBD.Desconectar();
            return curriculo;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    
}
//    TESTAR METODOS
//    public static void main(String[] args) throws SQLException, ParseException {
//        DAO_Curriculo a = new DAO_Curriculo();
//        Curriculo curriculo = new Curriculo();
//
//        java.sql.Date dataSql = ConverteData.converter("03/05/1983");
//
//        curriculo.setNomeEstagiario("Messi");
//        curriculo.setCpf("12345");
//        curriculo.setCurso("futebol");
//        curriculo.setLogradouro("argentina");
//        curriculo.setDataNascimento(dataSql);
//
//        curriculo.setSexo("masculino");
//        curriculo.setAreaAtuacao("esportes");
//        curriculo.setTurnoTrabalho("noite");
//        curriculo.setIdiomas("inglês, espanhol, alemão");
//        curriculo.setInformatica("básico");
//
//        curriculo.setExpProfissional("jogou no barcelona");
//        curriculo.setCursosExtras("nenhum");
//        curriculo.setInformacoesAdicionais("melhor do mundo");
//
//        a.salvar(curriculo);
//        a.atualizar(curriculo, 7);
//        a.excluir();
//    }
