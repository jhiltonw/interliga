/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.modelo;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Igor
 */
@ManagedBean(name = "estagiarioPreCadastro")
@SessionScoped

public class EstagiarioPreCadastro {

    private int idPreCad;
    private String nomeEstagiario;
    private String cpf;
    private String numMatricula;
    private String situacao;
    private String turno;
    private String curso;

    public EstagiarioPreCadastro(int idPreCad, String nomeEstagiario, String cpf, String numMatricula, String situacao, String turno, String curso) {
        this.idPreCad = idPreCad;
        this.nomeEstagiario = nomeEstagiario;
        this.cpf = cpf;
        this.numMatricula = numMatricula;
        this.situacao = situacao;
        this.turno = turno;
        this.curso = curso;
    }

    public EstagiarioPreCadastro() {

    }

    public int getIdPreCad() {
        return idPreCad;
    }

    public void setIdPreCad(int idPreCad) {
        this.idPreCad = idPreCad;
    }

    public String getNomeEstagiario() {
        return nomeEstagiario;
    }

    public void setNomeEstagiario(String nomeEstagiario) {
        this.nomeEstagiario = nomeEstagiario;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(String numMatricula) {
        this.numMatricula = numMatricula;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

}
