/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.modelo;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "estagiario")
@SessionScoped

public class Estagiario {

    private int idEstagiario;
    private String nomeEstagiario;
    private String cpf;
    private String rg;
    private String numMatricula;
    private String curso;
    private String universidade;
    private String turnoEstudo;
    private String sexo;
    private String email;
    private String senha;
    private String situacao;
    private String tipo;

    //SÃO VARIÁVEIS GENÉRICAS USADAS APENAS NO MÉTODO "EDITAR SENHA"
    private String aux1;
    private String aux2;

    public Estagiario(String email, String senha) {
        this.email = email;
        this.senha = senha;
    }

    public Estagiario() {

    }

    public int getIdEstagiario() {
        return idEstagiario;
    }

    public void setIdEstagiario(int idEstagiario) {
        this.idEstagiario = idEstagiario;
    }

    public String getNomeEstagiario() {
        return nomeEstagiario;
    }

    public void setNomeEstagiario(String nomeEstagiario) {
        this.nomeEstagiario = nomeEstagiario;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getNumMatricula() {
        return numMatricula;
    }

    public void setNumMatricula(String numMatricula) {
        this.numMatricula = numMatricula;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getUniversidade() {
        return universidade;
    }

    public void setUniversidade(String universidade) {
        this.universidade = universidade;
    }

    public String getTurnoEstudo() {
        return turnoEstudo;
    }

    public void setTurnoEstudo(String turnoEstudo) {
        this.turnoEstudo = turnoEstudo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getAux1() {
        return aux1;
    }

    public void setAux1(String aux1) {
        this.aux1 = aux1;
    }

    public String getAux2() {
        return aux2;
    }

    public void setAux2(String aux2) {
        this.aux2 = aux2;
    }

}
