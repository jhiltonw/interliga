/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.modelo;

import java.sql.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Igor
 */
@ManagedBean(name = "curriculo")
@SessionScoped

public class Curriculo {

    private int idCurriculo;
    private String nomeEstagiario;
    private String cpf;
    private String curso;
    private String estado;
    private String cidade;
    private String bairro;
    private String rua;
    private String numeroCasa;

    private String dataNascimento; // ficar atento nesse campo, pq ele pode causar problemas devido a conversão do date do java para o date do sql
                                   // o date usado aqui é o date sql, por isso antes de setar essa informação é necessário converter o date do java para esse date que é usado aqui
    private java.sql.Date dataFinal;
    private String sexo;
    private String areaAtuacao;
    private String turnoTrabalho;
    private String idiomas;
    private String informatica;
    private String expProfissional;
    private String cursosExtras;
    private String informacoesAdicionais;
    private int idEstagiarioFK;

    public Curriculo() {

    }

    public int getIdCurriculo() {
        return idCurriculo;
    }

    public void setIdCurriculo(int idCurriculo) {
        this.idCurriculo = idCurriculo;
    }

    public String getNomeEstagiario() {
        return nomeEstagiario;
    }

    public void setNomeEstagiario(String nomeEstagiario) {
        this.nomeEstagiario = nomeEstagiario;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getAreaAtuacao() {
        return areaAtuacao;
    }

    public void setAreaAtuacao(String areaAtuacao) {
        this.areaAtuacao = areaAtuacao;
    }

    public String getTurnoTrabalho() {
        return turnoTrabalho;
    }

    public void setTurnoTrabalho(String turnoTrabalho) {
        this.turnoTrabalho = turnoTrabalho;
    }

    public String getIdiomas() {
        return idiomas;
    }

    public void setIdiomas(String idiomas) {
        this.idiomas = idiomas;
    }

    public String getInformatica() {
        return informatica;
    }

    public void setInformatica(String informatica) {
        this.informatica = informatica;
    }

    public String getExpProfissional() {
        return expProfissional;
    }

    public void setExpProfissional(String expProfissional) {
        this.expProfissional = expProfissional;
    }

    public String getCursosExtras() {
        return cursosExtras;
    }

    public void setCursosExtras(String cursosExtras) {
        this.cursosExtras = cursosExtras;
    }

    public String getInformacoesAdicionais() {
        return informacoesAdicionais;
    }

    public void setInformacoesAdicionais(String informacoesAdicionais) {
        this.informacoesAdicionais = informacoesAdicionais;
    }

    public int getIdEstagiarioFK() {
        return idEstagiarioFK;
    }

    public void setIdEstagiarioFK(int idEstagiarioFK) {
        this.idEstagiarioFK = idEstagiarioFK;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

}
