/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Curriculo;
import br.com.interliga.dao.DAO_Solicitacao;
import br.com.interliga.modelo.Curriculo;
import br.com.interliga.util.ConverteData;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import validacao.CPF;

@ManagedBean(name = "CrudCurriculo")
@SessionScoped

public class CurriculoBean {

    
    //ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "ESTAGIARIO" PARA A TELA DE "CRIAÇÃO DE CURRICULO"
    public void salvarCurriculoP1(Integer idEstagiario) {

        DAO_Curriculo dao = new DAO_Curriculo();

        //VERIFICA SE O USUÁRIO JÁ POSSUI UM CURRICULO
        //CASO "SIM" É EXIBIDA UMA MENSAGEM E NÃO PERMITE QUE O USUÁRIO CADASTRE OUTRO CURRÍCULO
        //CASO "NÃO" CHAMA A TELA DE CRIAÇÃO DE CURRÍCULO
        if (dao.existeCurriculo(idEstagiario)) {

            //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Você já possui um currículo cadastrado!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("curriculo.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            Curriculo aux = new Curriculo();
            dao.preencheDadosCurr1(aux, idEstagiario);

            FacesContext fc = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
            session.setAttribute("curriculo", aux);

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("NovoCurriculo.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO "SALVA" DE FATO OS DADOS DO "CURRICULO" NO BANCO DE DADOS
    public void salvarCurriculoP2(Curriculo curriculo, int idEstagiario) {

        if ((!curriculo.getEstado().equals(""))
                && (!curriculo.getCidade().equals(""))
                && (!curriculo.getBairro().equals(""))
                && (!curriculo.getRua().equals(""))
                && (!curriculo.getNumeroCasa().equals(""))
                && (!curriculo.getDataNascimento().equals(""))
                && (!curriculo.getIdiomas().equals(""))) {

            DAO_Curriculo dao = new DAO_Curriculo();

            try {
                //CONVERTE A DATA QUE ESTA NA TELA PARA "DATA-SQL" QUE VAI PARA O BANCO DE DADOS
                java.sql.Date dtConvertida = ConverteData.converterParaDate(curriculo.getDataNascimento());
                curriculo.setDataFinal(dtConvertida);

                dao.salvar(curriculo, idEstagiario);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudCurriculo");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("curriculo");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Currículo salvo com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("PagEstudante.xhtml");

            } catch (ParseException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (curriculo.getEstado().equals("")) {
                    FacesUtil.MensagemErro("Estado vazio!!!");
                }
                if (curriculo.getCidade().equals("")) {
                    FacesUtil.MensagemErro("Cidade vazia!!!");
                }
                if (curriculo.getBairro().equals("")) {
                    FacesUtil.MensagemErro("Bairro vazio!!!");
                }
                if (curriculo.getRua().equals("")) {
                    FacesUtil.MensagemErro("Rua vazia!!!");
                }
                if (curriculo.getNumeroCasa().equals("")) {
                    FacesUtil.MensagemErro("NúmeroCasa vazio!!!");
                }
                if (curriculo.getDataNascimento().equals("")) {
                    FacesUtil.MensagemErro("Data de Nascimento vazia!!!");
                }
                if (curriculo.getIdiomas().equals("")) {
                    FacesUtil.MensagemErro("Idiomas vazios!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("NovoCurriculo.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
//ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "CURRICULO" PARA A TELA DE "EDICÃO DE CURRICULO"
    public void editarCurriculoP1(Integer idEstagiario) {

        DAO_Curriculo dao = new DAO_Curriculo();

        //VERIFICA SE O USUÁRIO JÁ POSSUI UM CURRICULO
        //CASO "SIM" CHAMA A TELA DE EDIÇÃO DE CURRÍCULO
        //CASO "NÃO" É EXIBIDA UMA MENSAGEM E NÃO PERMITE QUE O USUÁRIO EDITE UM CURRÍCULO QUE NÃO EXISTE 
        if (dao.existeCurriculo(idEstagiario)) {
            Curriculo aux = new Curriculo();
            dao.preencheDadosCurr2(aux, idEstagiario);

            FacesContext fc = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
            session.setAttribute("curriculo", aux);

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarCurriculo.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Você ainda não possui um currículo cadastrado, por isso não pode editá-lo!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("curriculo.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO "ATUALIZA" DE FATO OS DADOS DO "CURRICULO" NO BANCO DE DADOS
    public void editarCurriculoP2(Curriculo curriculo) {

        if ((!curriculo.getEstado().equals(""))
                && (!curriculo.getCidade().equals(""))
                && (!curriculo.getBairro().equals(""))
                && (!curriculo.getRua().equals(""))
                && (!curriculo.getNumeroCasa().equals(""))
                && (!curriculo.getDataNascimento().equals(""))
                && (!curriculo.getIdiomas().equals(""))) {

            DAO_Curriculo dao = new DAO_Curriculo();

            try {
                //CONVERTE A DATA QUE ESTA NA TELA PARA "DATA-SQL" QUE VAI PARA O BANCO DE DADOS
                java.sql.Date dtConvertida = ConverteData.converterParaDate(curriculo.getDataNascimento());
                curriculo.setDataFinal(dtConvertida);

                dao.atualizar(curriculo);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudCurriculo");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("curriculo");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Currículo atualizado com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("PagEstudante.xhtml");

            } catch (ParseException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (curriculo.getEstado().equals("")) {
                    FacesUtil.MensagemErro("Estado vazio!!!");
                }
                if (curriculo.getCidade().equals("")) {
                    FacesUtil.MensagemErro("Cidade vazia!!!");
                }
                if (curriculo.getBairro().equals("")) {
                    FacesUtil.MensagemErro("Bairro vazio!!!");
                }
                if (curriculo.getRua().equals("")) {
                    FacesUtil.MensagemErro("Rua vazia!!!");
                }
                if (curriculo.getNumeroCasa().equals("")) {
                    FacesUtil.MensagemErro("NúmeroCasa vazio!!!");
                }
                if (curriculo.getDataNascimento().equals("")) {
                    FacesUtil.MensagemErro("Data de Nascimento vazia!!!");
                }
                if (curriculo.getIdiomas().equals("")) {
                    FacesUtil.MensagemErro("Idiomas vazios!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarCurriculo.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO SERVE PARA FILTRAR OS CURRICULOS E SETÁ-LOS NA SESSÃO PARA QUE POSSAM SER EXIBIDOS NA TELA
    public void filtrarCurriculos(String areaAtuacao, String turnoTrabalho) {

        DAO_Curriculo dao = new DAO_Curriculo();
        listarUsuarioBean listagem = new listarUsuarioBean();

        listagem.setListaCurriculo(dao.pesquisarCurriculosPor(areaAtuacao, turnoTrabalho));

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("listagem", listagem);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("SolicitarEst_Emp.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //PEGA OS DADOS DE UM CURRICULO ESCOLHIDO E SETA ELES NA SESSÃO E ENCAMINHA PARA OUTRA PÁGINA
    //MAS SÓ ENCAMINHA CASO A EMPRESA AINDA NÃO TENHA SOLICITADO O ESTAGIÁRIO EM QUESTÃO
    public void detalhesCurriculo(Curriculo curriculo, Integer idEmpresa) {

        DAO_Solicitacao daoS = new DAO_Solicitacao();

        if (daoS.existeSolicitacao(idEmpresa, curriculo.getIdEstagiarioFK())) {

            //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Você já solicitou esse estagiário, por favor verifique sua lista de solicitações!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("SolicitarEst_Emp.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            DAO_Curriculo daoC = new DAO_Curriculo();
            Curriculo aux = new Curriculo();

            aux = daoC.pesquisarPorId(curriculo.getIdCurriculo());

            FacesContext fc = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
            session.setAttribute("curriculo", aux);

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("detalhesCurriculo.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "detalhesCurriculo" para a PÁGINA "SolicitarEst_Emp"
    public void cancelar() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("curriculo");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("SolicitarEst_Emp.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "FormPreCadEst" ou "EditarPreCad" para a PÁGINA "listaPreCad"
    public void cancelarSE() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("curriculo");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudCurriculo");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("PagEstudante.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
