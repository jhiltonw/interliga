package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Administrador;
import br.com.interliga.modelo.Administrador;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import validacao.CPF;

@ManagedBean(name = "CrudAdm")
@SessionScoped
public class AdministradorBean {

    
    //ESSE METODO "SALVA" DE FATO OS DADOS DO "ADMINISTRADOR" NO BANCO DE DADOS
    public void salvarAdm(Administrador adm) {

        String cpf = adm.getCpf();
        CPF pf = new CPF(cpf);

        //FAZ A VALIDAÇÃO DE TODOS OS CAMPOS E SALVA OS DADOS NO BANCO DE DADOS
        //MAS SÓ SALVA DE FOR INFORMADO UM "CPF" VÁLIDO E O RESTANTE DOS "CAMPOS ESTAR PREENCHIDO"
        //CASO SEJA INFORMADO UM "CPF" INVÁLIDO O TENHA ALGUM CAMPO EM BRANCO É "RECARREGADA A PÁGINA" E EXIBIDA "UMA MENSAGEM DE ERRO"
        if ((pf.isCPF())
                && (!adm.getNome().equals(""))
                && (!adm.getRg().equals(""))
                && (!adm.getEmail().equals(""))
                && (!adm.getSenha().equals(""))) {

            DAO_Administrador dao = new DAO_Administrador();

            try {
                adm.setTipo("Administrador");
                dao.salvar(adm);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudAdm");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("administrador");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Novo Administrador salvo com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("listaAdm.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inválido ou vazio!!!");
                }
                if (adm.getNome().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (adm.getRg().equals("")) {
                    FacesUtil.MensagemErro("RG vazio!!!");
                }
                if (adm.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }
                if (adm.getSenha().equals("")) {
                    FacesUtil.MensagemErro("Senha vazia!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("cadastroAdministrador.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "ADMINISTRADOR" PARA A TELA DE "EDIÇÃO"
    public void editarAdmP1(Administrador adm) {

        Administrador aux = new Administrador();
        aux = adm;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("administrador", aux);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("EditarAdm.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //ESSE METODO "ATUALIZA" DE FATO OS DADOS DO "ADMINISTRADOR" NO BANCO DE DADOS
    public void editarAdmP2(Administrador adm, Integer id) {

        String cpf = adm.getCpf();
        CPF pf = new CPF(cpf);

        //FAZ A VALIDAÇÃO DE TODOS OS CAMPOS E ATUALIZA OS DADOS NO BANCO DE DADOS
        //MAS SÓ ATUALIZA SE O "CPF" CONTINUAR VÁLIDO E O RESTANTE DOS "CAMPOS ESTAR PREENCHIDO"
        //CASO SEJA INFORMADO UM "CPF" INVÁLIDO O TENHA ALGUM CAMPO EM BRANCO É "RECARREGADA A PÁGINA" E EXIBIDA "UMA MENSAGEM DE ERRO"
        if ((pf.isCPF())
                && (!adm.getNome().equals(""))
                && (!adm.getRg().equals(""))
                && (!adm.getEmail().equals(""))) {

            DAO_Administrador dao = new DAO_Administrador();

            try {
                dao.atualizar(adm, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudAdm");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("administrador");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Dados atualizados com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("listaAdm.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inválido ou vazio!!!");
                }
                if (adm.getNome().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (adm.getRg().equals("")) {
                    FacesUtil.MensagemErro("RG vazio!!!");
                }
                if (adm.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }
                if (adm.getSenha().equals("")) {
                    FacesUtil.MensagemErro("Senha vazia!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarAdm.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "ADMINISTRADOR" PARA A TELA DE "EDIÇÃO DE SENHA"
    //OBSERVAÇÃO: NA TELA DE EDIÇÃO DE SENHA - SERÁ APENAS NECESSÁRIO O ID DO ADMINISTRADOR QUE TERÁ SUA SENHA EDITADA
    public void editarSenhaAdmP1(Administrador adm) {

        Administrador aux = new Administrador();
        aux = adm;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("administrador", aux);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("EditarSenhaAdm.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public void editarSenhaAdmP2(Administrador administrador, Integer id) {
        DAO_Administrador dao = new DAO_Administrador();

        if (administrador.getAux1().equals(administrador.getAux2())) {
            try {
                administrador.setSenha(administrador.getAux1());
                dao.atualizarSenha(administrador, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("administrador");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudAdm");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Senha atualizada com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("listaAdm.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemErro("Senhas informadas são distintas!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarSenhaAdm.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //EXCLUIR
    public void excluirAdm(Administrador adm) {
        DAO_Administrador dao = new DAO_Administrador();

        try {
            dao.excluir(adm);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Cadastro excluído com sucesso!!!");
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaAdm.xhtml");

        } catch (SQLException ex) {
            Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AdministradorBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "FormPreCadEst" ou "EditarPreCad" para a PÁGINA "listaPreCad"
    public void cancelar() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("administrador");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudAdm");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaAdm.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
