/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Administrador;
import br.com.interliga.dao.DAO_Empresa;
import br.com.interliga.dao.DAO_Estagiario;
import br.com.interliga.modelo.Administrador;
import br.com.interliga.modelo.Empresa;
import br.com.interliga.modelo.Estagiario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;


@ManagedBean(name = "conferePessoa")
@SessionScoped

public class ValidaLoginBean {

    private Estagiario estagiarioLogado = null;
    private Empresa empresaLogado = null;
    private Administrador admLogado = null;
    private boolean log = false;

    private String email;
    private String senha;
    private String tipo;

    public ValidaLoginBean() {

    }

    public Estagiario getEstagiarioLogado() {
        return estagiarioLogado;
    }

    public void setEstagiarioLogado(Estagiario estagiarioLogado) {
        this.estagiarioLogado = estagiarioLogado;
    }

    public Empresa getEmpresaLogado() {
        return empresaLogado;
    }

    public void setEmpresaLogado(Empresa empresaLogado) {
        this.empresaLogado = empresaLogado;
    }

    public Administrador getAdmLogado() {
        return admLogado;
    }

    public void setAdmLogado(Administrador admLogado) {
        this.admLogado = admLogado;
    }

    public boolean isLog() {
        return log;
    }

    public void setLog(boolean log) {
        this.log = log;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
    //METODO LOGAR USUARIO NO SISTEMA
    public void logar() {

        if (tipo.equals("Administrador")) {
            DAO_Administrador dao = new DAO_Administrador();
            Administrador aux = dao.autenticar(new Administrador(email, senha));

            if (!aux.getNome().equals("")) {
                log = true;
                admLogado = aux;

                //Aqui limpa outros usuários de outros tipos que ainda podem estar na sessão
                empresaLogado = null;
                estagiarioLogado = null;

                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("PagAdm.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else if (tipo.equals("Empresa")) {
            DAO_Empresa dao = new DAO_Empresa();
            Empresa aux = dao.autenticar(new Empresa(email, senha));

            if (!aux.getNomeEmpresa().equals("")) {
                log = true;
                empresaLogado = aux;

                //Aqui limpa outros usuários de outros tipos que ainda podem estar na sessão
                estagiarioLogado = null;
                admLogado = null;

                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("PagEmpresa.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else if (tipo.equals("Estagiario")) {
            DAO_Estagiario dao = new DAO_Estagiario();
            Estagiario aux = dao.autenticar(new Estagiario(email, senha));

            if (!aux.getNomeEstagiario().equals("")) {
                log = true;
                estagiarioLogado = aux;

                //Aqui limpa outros usuários de outros tipos que ainda podem estar na sessão
                admLogado = null;
                empresaLogado = null;

                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("PagEstudante.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    
    //METODO DE VERIFICAR LOGIN - PARA PAGINAS QUE TODOS OS USUARIOS PODEM ACESSAR
    public void testeLog() {
        if (this.log == false) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //METODO DE VERIFICAR LOGIN - PARA PAGINAS QUE APENAS ADMINISTRADORES PODEM ACESSAR
    public void testeLogAdm() {
        if (this.log == false) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (this.admLogado == null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    
    //METODO DE VERIFICAR LOGIN - PARA PAGINAS QUE APENAS EMPRESAS PODEM ACESSAR
    public void testeLogEmpresa() {
        if (this.log == false) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (this.empresaLogado == null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    
    //METODO DE VERIFICAR LOGIN - PARA PAGINAS QUE APENAS ESTAGIARIOS PODEM ACESSAR
    public void testeLogEstagiario() {
        if (this.log == false) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (this.estagiarioLogado == null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(ValidaLoginBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void desloga() throws IOException{
        //Contexto da Aplicação
        FacesContext conext = FacesContext.getCurrentInstance();
        //Verifica a sessao e a grava na variavel
        HttpSession session = (HttpSession) conext.getExternalContext().getSession(false);
        //Fecha/Destroi sessao
        session.invalidate();
        
        FacesContext.getCurrentInstance().getExternalContext().redirect("inicio.xhtml");
    }

}
