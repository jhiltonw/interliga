/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Empresa;
import br.com.interliga.dao.DAO_Estagiario;
import br.com.interliga.dao.DAO_Solicitacao;
import br.com.interliga.modelo.Curriculo;
import br.com.interliga.modelo.SolicitacaoEmpEst;
import br.com.interliga.util.EnviarEmail;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "CrudSolicitacao")
@SessionScoped

public class SolicitacaoBean {

    public void salvarSolicitacao(Integer idEmpresa, String nomeEmpresa, Curriculo curriculo) {

        DAO_Solicitacao dao = new DAO_Solicitacao();
        SolicitacaoEmpEst solicitacao = new SolicitacaoEmpEst();

        solicitacao.setIdEmpresa(idEmpresa);
        solicitacao.setIdEstagiario(curriculo.getIdEstagiarioFK());

        solicitacao.setNomeEmpresa(nomeEmpresa);
        solicitacao.setCnpj(DAO_Empresa.buscaCnpj(idEmpresa));

        solicitacao.setNomeEstagiario(curriculo.getNomeEstagiario());
        solicitacao.setNumMatricula(DAO_Estagiario.buscaNumMatricula(curriculo.getIdEstagiarioFK()));

        solicitacao.setAreaAtuacao(curriculo.getAreaAtuacao());

        try {
            dao.salvar(solicitacao);
            
            String emailEmpresa = DAO_Empresa.buscaEmail(idEmpresa);
            String emailEstagiario = DAO_Estagiario.buscaEmail(curriculo.getIdEstagiarioFK());
            
            EnviarEmail.enviar(emailEmpresa, "Sua solicitação foi enviada para o estagiário " + curriculo.getNomeEstagiario() + ".");
            EnviarEmail.enviar(emailEstagiario, "A empresa " + nomeEmpresa + " fez uma solicitação de seu currículo.");
            
            //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("curriculo");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudSolicitacao");

            //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Solicitação efetuada com sucesso!!!");
            FacesContext.getCurrentInstance().getExternalContext().redirect("SolicitarEst_Emp.xhtml");

        } catch (SQLException ex) {
            Logger.getLogger(SolicitacaoBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SolicitacaoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //CANCELA UMA "SOLICITACAO" DE UMA "EMPRESA", ESSA MÉTODO SÓ PODE SER EXECUTADO POR UMA "EMPRESA"
    public void excluirSolicitacao(SolicitacaoEmpEst solicitacao) {
        DAO_Solicitacao dao = new DAO_Solicitacao();

        try {
            dao.excluir(solicitacao);
            FacesContext.getCurrentInstance().getExternalContext().redirect("SolicitacoesFeitas_Emp.xhtml");
        } catch (SQLException ex) {
            Logger.getLogger(SolicitacaoBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SolicitacaoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
