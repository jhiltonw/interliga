package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Estagiario;
import br.com.interliga.modelo.Estagiario;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import validacao.CPF;

@ManagedBean(name = "CrudEstagiario")
@SessionScoped
public class EstagiarioBean {

    
    //ESSE METODO "SALVA" DE FATO OS DADOS DO "ESTAGIÁRIO" NO BANCO DE DADOS
    public void salvarEstagiario(Estagiario estagiario) {

        String cpf = estagiario.getCpf();
        CPF pf = new CPF(cpf);

        if ((pf.isCPF())
                && (!estagiario.getNomeEstagiario().equals(""))
                && (!estagiario.getRg().equals(""))
                && (!estagiario.getNumMatricula().equals(""))
                && (!estagiario.getCurso().equals(""))
                && (!estagiario.getUniversidade().equals(""))
                && (!estagiario.getEmail().equals(""))
                && (!estagiario.getSenha().equals(""))) {

            DAO_Estagiario dao = new DAO_Estagiario();

            try {
                estagiario.setTipo("Estagiario");
                dao.salvar(estagiario);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEstagiario");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiario");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Cadastro salvo com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inválido ou vazio!!!");
                }
                if (estagiario.getNomeEstagiario().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (estagiario.getRg().equals("")) {
                    FacesUtil.MensagemErro("RG vazio!!!");
                }
                if (estagiario.getNumMatricula().equals("")) {
                    FacesUtil.MensagemErro("Matrícula vazia!!!");
                }
                if (estagiario.getCurso().equals("")) {
                    FacesUtil.MensagemErro("Curso vazio!!!");
                }
                if (estagiario.getUniversidade().equals("")) {
                    FacesUtil.MensagemErro("Universidade vazia!!!");
                }
                if (estagiario.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }
                if (estagiario.getSenha().equals("")) {
                    FacesUtil.MensagemErro("Senha vazia!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("FormEstudante.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    ////ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "ESTAGIÁRIO" PARA A TELA DE "EDIÇÃO"
    //OBS: NESSE CASO, ESSE MÉTODO É UTILZADO QUANDO O PRÓPRIO "ESTAGIÁRIO" DESEJA EDITAR OS SEUS DADOS
    public void editarEstagiarioP1(Integer idEstagiario) {

        DAO_Estagiario dao = new DAO_Estagiario();
        Estagiario aux = new Estagiario();

        dao.preencheDadosEst1(aux, idEstagiario);

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("estagiario", aux);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("EditarEstagiario.xhtml");

        } catch (IOException ex) {
            Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //ESSE METODO "ATUALIZA" DE FATO OS DADOS DO "ESTAGIÁRIO" NO BANCO DE DADOS
    public void editarEstagiarioP2(Estagiario estagiario, Integer id) {

        String cpf = estagiario.getCpf();
        CPF pf = new CPF(cpf);

        if ((pf.isCPF())
                && (!estagiario.getNomeEstagiario().equals(""))
                && (!estagiario.getRg().equals(""))
                && (!estagiario.getNumMatricula().equals(""))
                && (!estagiario.getCurso().equals(""))
                && (!estagiario.getUniversidade().equals(""))
                && (!estagiario.getEmail().equals(""))) {

            DAO_Estagiario dao = new DAO_Estagiario();

            try {
                dao.atualizar(estagiario, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiario");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEstagiario");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Cadastro atualizado com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("meuCadEst.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inválido ou vazio!!!");
                }
                if (estagiario.getNomeEstagiario().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (estagiario.getRg().equals("")) {
                    FacesUtil.MensagemErro("RG vazio!!!");
                }
                if (estagiario.getNumMatricula().equals("")) {
                    FacesUtil.MensagemErro("Matrícula vazia!!!");
                }
                if (estagiario.getCurso().equals("")) {
                    FacesUtil.MensagemErro("Curso vazio!!!");
                }
                if (estagiario.getUniversidade().equals("")) {
                    FacesUtil.MensagemErro("Universidade vazia!!!");
                }
                if (estagiario.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarEstagiario.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    public void editarSenha(Estagiario estagiario, Integer id) {
        DAO_Estagiario dao = new DAO_Estagiario();

        if (estagiario.getAux1().equals(estagiario.getAux2())) {
            try {
                estagiario.setSenha(estagiario.getAux1());
                dao.atualizarSenha(estagiario, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiario");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEstagiario");

                //ESSE DADO É REMOVIDO PARA DESLOGAR AUTOMATICAMENTE O USUÁRIO, APÓS A ALTERAÇÃO DA SENHA
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("conferePessoa");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Senha atualizada com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemErro("Senhas informadas são distintas!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarSenhaEst.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //EXCLUIR
    public void excluirEstagiario(Estagiario estagiario) {
        DAO_Estagiario dao = new DAO_Estagiario();

        try {
            dao.excluir(estagiario);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Estagiário excluído com sucesso!!!");
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaEstagiario.xhtml");

        } catch (SQLException ex) {
            Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            //FacesUtil.MensagemErro("Deu Errado: " + ex);
        } catch (IOException ex) {
            Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "FormEstudante" para a PÁGINA "cadastrar"
    public void cancelarS() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiario");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEstagiario");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("cadastrar.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    //METODO PARA REDIRECIONAR DA PÁGINA "EditarEstagiario" para a PÁGINA "meuCadEst"
    public void cancelarE() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiario");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEstagiario");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("meuCadEst.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
