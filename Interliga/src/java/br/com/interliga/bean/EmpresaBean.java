package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Empresa;
import br.com.interliga.modelo.Empresa;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import validacao.CNPJ;

@ManagedBean(name = "CrudEmpresa")
@SessionScoped

public class EmpresaBean {

    
    //ESSE METODO "SALVA" DE FATO OS DADOS DA "EMPRESA" NO BANCO DE DADOS
    public void salvarEmpresa(Empresa empresa) {

        String cnpj = empresa.getCnpj();
        CNPJ pf = new CNPJ(cnpj);

        if ((pf.isCNPJ())
                && (!empresa.getNomeEmpresa().equals(""))
                && (!empresa.getResponsavel().equals(""))
                && (!empresa.getEstado().equals(""))
                && (!empresa.getCidade().equals(""))
                && (!empresa.getBairro().equals(""))
                && (!empresa.getRua().equals(""))
                && (!empresa.getNumeroCasa().equals(""))
                && (!empresa.getTelefone().equals(""))
                && (!empresa.getAreaAtuacao().equals(""))
                && (!empresa.getEmail().equals(""))
                && (!empresa.getSenha().equals(""))) {

            DAO_Empresa dao = new DAO_Empresa();

            try {
                empresa.setTipo("Empresa");
                dao.salvar(empresa);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEmpresa");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("empresa");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("O cadastro de sua empresa foi salvo com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);

            } catch (IOException ex) {
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCNPJ()) {
                    FacesUtil.MensagemErro("CNPJ inválido ou vazio!!!");
                }
                if (empresa.getNomeEmpresa().equals("")) {
                    FacesUtil.MensagemErro("Nome da Empresa vazio!!!");
                }
                if (empresa.getResponsavel().equals("")) {
                    FacesUtil.MensagemErro("Nome do Responsável vazio!!!");
                }
                if (empresa.getEstado().equals("")) {
                    FacesUtil.MensagemErro("Estado vazio!!!");
                }
                if (empresa.getCidade().equals("")) {
                    FacesUtil.MensagemErro("Cidade vazia!!!");
                }
                if (empresa.getBairro().equals("")) {
                    FacesUtil.MensagemErro("Bairro vazio!!!");
                }
                if (empresa.getRua().equals("")) {
                    FacesUtil.MensagemErro("Rua vazio!!!");
                }
                if (empresa.getNumeroCasa().equals("")) {
                    FacesUtil.MensagemErro("Número da casa vazio!!!");
                }
                if (empresa.getTelefone().equals("")) {
                    FacesUtil.MensagemErro("Telefone vazio!!!");
                }
                if (empresa.getAreaAtuacao().equals("")) {
                    FacesUtil.MensagemErro("Áre de atuação vazia!!!");
                }
                if (empresa.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }
                if (empresa.getSenha().equals("")) {
                    FacesUtil.MensagemErro("Senha vazio!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("CadEmpresa.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DA "EMPRESA" PARA A TELA DE "EDIÇÃO"
    public void editarEmpresaP1(Integer idEmpresa) {

        DAO_Empresa dao = new DAO_Empresa();
        Empresa aux = new Empresa();

        dao.preencheDadosEmp(aux, idEmpresa);

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("empresa", aux);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("EditarEmpresa.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //ESSE METODO "ATUALIZA" DE FATO OS DADOS DA "EMPRESA" NO BANCO DE DADOS
    public void editarEmpresaP2(Empresa empresa, Integer id) {

        String cnpj = empresa.getCnpj();
        CNPJ pf = new CNPJ(cnpj);

        if ((pf.isCNPJ())
                && (!empresa.getNomeEmpresa().equals(""))
                && (!empresa.getResponsavel().equals(""))
                && (!empresa.getEstado().equals(""))
                && (!empresa.getCidade().equals(""))
                && (!empresa.getBairro().equals(""))
                && (!empresa.getRua().equals(""))
                && (!empresa.getNumeroCasa().equals(""))
                && (!empresa.getTelefone().equals(""))
                && (!empresa.getAreaAtuacao().equals(""))
                && (!empresa.getEmail().equals(""))) {

            DAO_Empresa dao = new DAO_Empresa();
            
            try {
                dao.atualizar(empresa, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("empresa");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEmpresa");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Cadastro atualizado com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("meuCadEmp.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            
            } catch (IOException ex) {  
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCNPJ()) {
                    FacesUtil.MensagemErro("CNPJ inválido ou vazio!!!");
                }
                if (empresa.getNomeEmpresa().equals("")) {
                    FacesUtil.MensagemErro("Nome da Empresa vazio!!!");
                }
                if (empresa.getResponsavel().equals("")) {
                    FacesUtil.MensagemErro("Nome do Responsável vazio!!!");
                }
                if (empresa.getEstado().equals("")) {
                    FacesUtil.MensagemErro("Estado vazio!!!");
                }
                if (empresa.getCidade().equals("")) {
                    FacesUtil.MensagemErro("Cidade vazia!!!");
                }
                if (empresa.getBairro().equals("")) {
                    FacesUtil.MensagemErro("Bairro vazio!!!");
                }
                if (empresa.getRua().equals("")) {
                    FacesUtil.MensagemErro("Rua vazio!!!");
                }
                if (empresa.getNumeroCasa().equals("")) {
                    FacesUtil.MensagemErro("Número da casa vazio!!!");
                }
                if (empresa.getTelefone().equals("")) {
                    FacesUtil.MensagemErro("Telefone vazio!!!");
                }
                if (empresa.getAreaAtuacao().equals("")) {
                    FacesUtil.MensagemErro("Áre de atuação vazia!!!");
                }
                if (empresa.getEmail().equals("")) {
                    FacesUtil.MensagemErro("Email vazio!!!");
                }
               
                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarEmpresa.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    public void editarSenha(Empresa empresa, Integer id) {
        DAO_Empresa dao = new DAO_Empresa();

        if (empresa.getAux1().equals(empresa.getAux2())) {
            try {
                empresa.setSenha(empresa.getAux1());
                dao.atualizarSenha(empresa, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("empresa");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEmpresa");

                //ESSE DADO É REMOVIDO PARA DESLOGAR AUTOMATICAMENTE O USUÁRIO, APÓS A ALTERAÇÃO DA SENHA
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("conferePessoa");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Senha atualizada com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemErro("Senhas informadas são distintas!!!");

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarSenhaEmp.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(EstagiarioBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    //EXCLUIR
    public void excluirEmpresa(Empresa empresa) {
        DAO_Empresa dao = new DAO_Empresa();

        try {
            dao.excluir(empresa);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Empresa excluída com sucesso!!!");
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaEmpresas.xhtml");

        } catch (SQLException ex) {
            Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
            //FacesUtil.MensagemErro("Deu Errado: " + ex);
        } catch (IOException ex) {
            Logger.getLogger(EmpresaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "CadEmpresa" para a PÁGINA "cadastrar"
    public void cancelarS() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("empresa");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEmpresa");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("cadastrar.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "EditarEmpresa" para a PÁGINA "meuCadEmp"
    public void cancelarE() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("empresa");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudEmpresa");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("meuCadEmp.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
