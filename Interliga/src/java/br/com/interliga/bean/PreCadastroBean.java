package br.com.interliga.bean;

import br.com.interliga.dao.DAO_PreCadastro;
import br.com.interliga.modelo.Estagiario;
import br.com.interliga.modelo.EstagiarioPreCadastro;
import br.com.interliga.util.FacesUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import validacao.CPF;

@ManagedBean(name = "CrudPreCadastro")
@SessionScoped

public class PreCadastroBean {

    
    //ESSE METODO "SALVA" DE FATO OS DADOS DO "PRÉ-CADASTRO" NO BANCO DE DADOS
    public void salvarPreCadastro(EstagiarioPreCadastro preCad) {

        
        String cpf = preCad.getCpf();
        CPF pf = new CPF(cpf);

        //FAZ A VALIDAÇÃO DE TODOS OS CAMPOS E SALVA OS DADOS NO BANCO DE DADOS
        //MAS SÓ SALVA DE FOR INFORMADO UM "CPF" VÁLIDO E O RESTANTE DOS "CAMPOS ESTAR PREENCHIDO"
        //CASO SEJA INFORMADO UM "CPF" INVÁLIDO O TENHA ALGUM CAMPO EM BRANCO É "RECARREGADA A PÁGINA" E EXIBIDA "UMA MENSAGEM DE ERRO"
        if ((pf.isCPF())
                && (!preCad.getNomeEstagiario().equals(""))
                && (!preCad.getNumMatricula().equals(""))
                && (!preCad.getCurso().equals(""))) {

            DAO_PreCadastro dao = new DAO_PreCadastro();

            try {
                dao.salvar(preCad);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudPreCadastro");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiarioPreCadastro");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Pré-Cadastro salvo com sucesso!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("listaPreCad.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inválido ou vazio!!!");
                }
                if (preCad.getNomeEstagiario().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (preCad.getNumMatricula().equals("")) {
                    FacesUtil.MensagemErro("Matrícula vazia!!!");
                }
                if (preCad.getCurso().equals("")) {
                    FacesUtil.MensagemErro("Curso vazio!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("FormPreCadEst.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //ESSE METODO É APENAS PARA TRANSPORTAR OS DADOS DO "PRÉ-CADASTRO" PARA A TELA DE "EDIÇÃO"
    public void editarPreCadP1(EstagiarioPreCadastro preCad) {

        EstagiarioPreCadastro aux = new EstagiarioPreCadastro();
        aux = preCad;

        FacesContext fc = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
        session.setAttribute("estagiarioPreCadastro", aux);

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("EditarPreCad.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //ESSE METODO "ATUALIZA" DE FATO OS DADOS DO "PRÉ-CADASTRO" NO BANCO DE DADOS
    public void editarPreCadP2(EstagiarioPreCadastro preCad, Integer id) {

        String cpf = preCad.getCpf();
        CPF pf = new CPF(cpf);

        //FAZ A VALIDAÇÃO DE TODOS OS CAMPOS E ATUALIZA OS DADOS NO BANCO DE DADOS
        //MAS SÓ ATUALIZA SE O "CPF" CONTINUAR VÁLIDO E O RESTANTE DOS "CAMPOS ESTAR PREENCHIDO"
        //CASO SEJA INFORMADO UM "CPF" INVÁLIDO O TENHA ALGUM CAMPO EM BRANCO É "RECARREGADA A PÁGINA" E EXIBIDA "UMA MENSAGEM DE ERRO"
        if ((pf.isCPF())
                && (!preCad.getNomeEstagiario().equals(""))
                && (!preCad.getNumMatricula().equals(""))
                && (!preCad.getCurso().equals(""))) {

            DAO_PreCadastro dao = new DAO_PreCadastro();

            try {
                dao.atualizar(preCad, id);

                //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiarioPreCadastro");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudPreCadastro");

                //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Pré-Cadastro atualizado com sucesso");
                FacesContext.getCurrentInstance().getExternalContext().redirect("listaPreCad.xhtml");

            } catch (SQLException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

                if (!pf.isCPF()) {
                    FacesUtil.MensagemErro("CPF inváldo ou vazio!!!");
                }
                if (preCad.getNomeEstagiario().equals("")) {
                    FacesUtil.MensagemErro("Nome vazio!!!");
                }
                if (preCad.getNumMatricula().equals("")) {
                    FacesUtil.MensagemErro("Matrícula vazia!!!");
                }
                if (preCad.getCurso().equals("")) {
                    FacesUtil.MensagemErro("Curso vazio!!!");
                }

                FacesContext.getCurrentInstance().getExternalContext().redirect("EditarPreCad.xhtml");

            } catch (IOException ex) {
                Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    //EXCLUIR
    public void excluirPreCad(EstagiarioPreCadastro preCad) {
        DAO_PreCadastro dao = new DAO_PreCadastro();

        try {
            dao.excluir(preCad);

            //PERMITE QUE SEJA MOSTRADA A MENSAGEM APÓS O REDIRECIONAMENTO DÁ PÁGINA
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            FacesUtil.MensagemIformativa("Pré-Cadastro excluído com sucesso");
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaPreCad.xhtml");

        } catch (SQLException ex) {
            System.err.println(ex);
            FacesUtil.MensagemErro("Não foi possível excluir o pré-cadastro do estagiário!");

        } catch (IOException ex) {
            Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    
    //VERIFICA SE O ESTAGIARIO JÁ TEM O PRÉ-CADASTRO, 
    //CASO ELE TENHA, O MESMO É REDIRECIONADO PARA A PÁGINA DE CADASTRO DEFINITIVO
    //DE ESTAGIARIO E SETA OS DADOS DO PRÉ-CADASTRO NO FORM DE CADASTRO DEFINITIVO
    public void validaPreCadastro(EstagiarioPreCadastro preCadastro) {

        DAO_PreCadastro dao = new DAO_PreCadastro();
        boolean cadastroLiberado = false;

        try {
            cadastroLiberado = dao.pesquisarMatricula(preCadastro.getNumMatricula());//VERIFICA SE O ESTAGIÁRIO JÁ TEM O PRÉ-CADASTRO

            if (cadastroLiberado) {
                Estagiario aux = new Estagiario();
                dao.preencheDadosEst(preCadastro.getNumMatricula(), aux);//SETA OS DADOS DO PRÉ-CADASTRO NUMA VARIÁVEL TEMPORÁRIA

                //SETA OS DADOS DO PRÉ-CADASTRO EM UMA VARIÁVEL DA SESSÃO
                FacesContext fc = FacesContext.getCurrentInstance();
                HttpSession session = (HttpSession) fc.getExternalContext().getSession(true);
                session.setAttribute("estagiario", aux);

                //LIMPA OS OBJETOS DA SESSÃO QUE NÃO SERÃO MAIS UTILIZADOS E EVITA QUE OS MESMOS CONTINUEM A SEREM EXIBIDOS NOS FORMS
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudPreCadastro");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiarioPreCadastro");

                //ENCAMINHA PARA A PÁGINA SEGUINTE
                FacesContext.getCurrentInstance().getExternalContext().redirect("FormEstudante.xhtml");

            } else {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                FacesUtil.MensagemIformativa("Número de matrícula inválido!!!");
                FacesContext.getCurrentInstance().getExternalContext().redirect("solicitaCadEst.xhtml");
            }

        } catch (IOException ex) {
            Logger.getLogger(PreCadastroBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    //METODO PARA REDIRECIONAR DA PÁGINA "FormPreCadEst" ou "EditarPreCad" para a PÁGINA "listaPreCad"
    public void cancelar() {

        //LIMPA OS DADOS DA SESSÃO PARA QUE OS MESMOS NÃO CONTINUEM A SEREM EXIBIDOS NOS FORMS
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("estagiarioPreCadastro");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("CrudPreCadastro");

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("listaPreCad.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(CurriculoBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
