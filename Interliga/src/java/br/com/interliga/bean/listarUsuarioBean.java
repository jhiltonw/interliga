/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.bean;

import br.com.interliga.dao.DAO_Administrador;
import br.com.interliga.dao.DAO_Empresa;
import br.com.interliga.dao.DAO_Estagiario;
import br.com.interliga.dao.DAO_PreCadastro;
import br.com.interliga.dao.DAO_Solicitacao;
import br.com.interliga.modelo.Administrador;
import br.com.interliga.modelo.Curriculo;
import br.com.interliga.modelo.Empresa;
import br.com.interliga.modelo.Estagiario;
import br.com.interliga.modelo.EstagiarioPreCadastro;
import br.com.interliga.modelo.SolicitacaoEmpEst;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "listagem")
@SessionScoped
public class listarUsuarioBean {

    private List<Administrador> listaAdm = new ArrayList<>();
    private List<Empresa> listaEmpresa = new ArrayList<>();
    private List<Estagiario> listaEstagiario = new ArrayList<>();
    private List<EstagiarioPreCadastro> listaPreCadEst = new ArrayList<>();
    private List<Curriculo> listaCurriculo = new ArrayList<>();

    private List<SolicitacaoEmpEst> listaSolicitacoes = new ArrayList<>(); // TODAS AS SOLICITAÇÕES
    private List<SolicitacaoEmpEst> listaSolicitFeitasEmp = new ArrayList<>();// AS SOLICITACOES FEITAS POR UMA DETERMINADA EMPRESA
    private List<SolicitacaoEmpEst> listaSolicitRecebidasEst = new ArrayList<>();// AS SOLICITAÇÕES RECEBIDAS PRO UM DETERMINADO ESTAGIÁRIO

    public listarUsuarioBean() {

    }

    //LISTAR ADMINISTRADOR
    public List<Administrador> getListaAdm() {
        DAO_Administrador dao = new DAO_Administrador();
        this.listaAdm = dao.listar();
        return listaAdm;
    }

    //LISTAR EMPRESA
    public List<Empresa> getListaEmpresa() {
        DAO_Empresa dao = new DAO_Empresa();
        this.listaEmpresa = dao.listar();
        return listaEmpresa;
    }

    //LISTAR ESTAGIARIO
    public List<Estagiario> getListaEstagiario() {
        DAO_Estagiario dao = new DAO_Estagiario();
        this.listaEstagiario = dao.listar();
        return listaEstagiario;
    }

    //LISTAR PRE-CADASTRO DE ESTAGIARIO
    public List<EstagiarioPreCadastro> getListaPreCadEst() {
        DAO_PreCadastro dao = new DAO_PreCadastro();
        this.listaPreCadEst = dao.listar();
        return listaPreCadEst;
    }

    public List<SolicitacaoEmpEst> getListaSolicitacoes() {
        DAO_Solicitacao dao = new DAO_Solicitacao();
        this.listaSolicitacoes = dao.listar();
        return listaSolicitacoes;
    }

    //LISTAR SOLICITACOES FEITAS POR UMA DETERMINADA EMPRESA
    public List<SolicitacaoEmpEst> getListaSolicitFeitasEmp(Integer idEmpresa) {
        DAO_Solicitacao dao = new DAO_Solicitacao();
        this.listaSolicitFeitasEmp = dao.solicitFeitasEmp(idEmpresa);
        return listaSolicitFeitasEmp;
    }

    //LISTAR SOLICITACOES RECEBIDAS POR UM DETERMINADO ESTAGIARIO
    public List<SolicitacaoEmpEst> getListaSolicitRecebidasEst(Integer idEstagiario) {
        DAO_Solicitacao dao = new DAO_Solicitacao();
        this.listaSolicitRecebidasEst = dao.solicitRecebidasEst(idEstagiario);
        return listaSolicitRecebidasEst;
    }

    //LISTAR CURRICULOS
    public List<Curriculo> getListaCurriculo() {
        return listaCurriculo;
    }

    //SETAR CURRICULOS
    public void setListaCurriculo(List<Curriculo> listaCurriculo) {
        this.listaCurriculo = listaCurriculo;
    }

}
