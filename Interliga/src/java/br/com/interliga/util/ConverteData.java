/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.interliga.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Igor
 */
public class ConverteData {

    //estou usando a notacao java.sql.Date, pq existem dois tipo de date aí a gente ou a ide pode bugar, entao achei melhor deixar
    //bem especificado qual date esta sendo usado
    //**********************Observação importante: o parametro strDate deve ser passado no formato dia/mes/ano************************
    
    //CONVERTE PARA java.sql.Date
    public static java.sql.Date converterParaDate(String strData) throws ParseException {
        Date objDate = new SimpleDateFormat("dd/MM/yyyy").parse(strData);
        java.sql.Date dataSql = new java.sql.Date(objDate.getTime());
        return dataSql;
    }

    //CONVERTE PARA STRING
    public static String converterParaString(java.sql.Date sqlData) {
        return new SimpleDateFormat("dd/MM/yyyy").format(sqlData);
    }

}
